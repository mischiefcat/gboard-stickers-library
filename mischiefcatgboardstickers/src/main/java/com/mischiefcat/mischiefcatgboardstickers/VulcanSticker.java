package com.mischiefcat.mischiefcatgboardstickers;

/**
 * A sticker that should be shown.
 */
public class VulcanSticker
{
    // The name of the stickers
    private final String name;

    // URL to the sticker image
    private final String imageUrl;

    // Keywords of the sticker
    private final String keywords[];

    /**
     * Creates a sticker isntance
     * @param argName Name of the sticker.
     * @param argImageUrl URL to the image asset.
     * @param argKeywords Keyword for the image.
     */
    public VulcanSticker(String argName, String argImageUrl, String argKeywords[])
    {
        this.name = argName;
        this.imageUrl = argImageUrl;
        this.keywords = argKeywords;
    }

    /**
     * Gets the name of the sticker.
     * @return The name of the sticker.
     */
    public String GetName()
    {
        return name;
    }

    /**
     * Gets the URL to the image asset.
     * @return The URL to the sticker image asset.
     */
    public String GetImageUrl()
    {
        return imageUrl;
    }

    /**
     * Gets the keywords of the sticker.
     * @return Keywords of the sticker
     */
    public String [] GetKeywords()
    {
        return keywords;
    }
}

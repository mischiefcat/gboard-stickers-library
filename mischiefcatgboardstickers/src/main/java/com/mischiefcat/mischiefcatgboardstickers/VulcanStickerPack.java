package com.mischiefcat.mischiefcatgboardstickers;

/**
 * A collection of stickers.
 */
public class VulcanStickerPack
{
    // The version of the sticker pack
    private int version;

    // Name of the pack
    private String packName;

    // Description of the pack
    private String packDescription;

    // Sticker to show as an icon
    private int featuredStickerIndex;

    // Stickers in the pack
    private VulcanSticker[] stickers;

    /**
     * Creates a sticker pack
     * @param argVersion Version of the sticker pack.
     * @param argPackName Name of the sticker pack.
     * @param argPackDescription Description of the sticker pack.
     * @param argFeaturedStickerIndex The index of the sticker to show as an icon.
     * @param argStickers Stickers in the pack.
     */
    public VulcanStickerPack(int argVersion, String argPackName, String argPackDescription, int argFeaturedStickerIndex, VulcanSticker[] argStickers)
    {
        this.version = argVersion;
        this.packName = argPackName;
        this.packDescription = argPackDescription;
        this.featuredStickerIndex = argFeaturedStickerIndex;
        this.stickers = argStickers;
    }

    /**
     * Gets the sticker pack version.
     * @return The version of the sticker pack
     */
    public int GetVersion()
    {
        return version;
    }

    /**
     * Gets the name of the sticker pack.
     * @return The name of the sticker pack.
     */
    public String GetPackName()
    {
        return packName;
    }

    /**
     * Gets the description of the sticker pack.
     * @return The description of the sticker pack.
     */
    public String GetPackDescription()
    {
        return packDescription;
    }

    /**
     * Gets the index of the sticker to use as the icon
     * @return The index of the sticker to use as the icon.
     */
    public int GetFeaturedStickerIndex()
    {
        return featuredStickerIndex;
    }

    /**
     * Gets the stickers in the sticker pack.
     * @return The stickers in the pack.
     */
    public VulcanSticker[] GetStickers()
    {
        return stickers;
    }
}

package com.mischiefcat.mischiefcatgboardstickers;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.JobIntentService;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.appindexing.FirebaseAppIndex;
import com.google.firebase.appindexing.FirebaseAppIndexingInvalidArgumentException;
import com.google.firebase.appindexing.Indexable;
import com.google.firebase.appindexing.builders.Indexables;
import com.google.firebase.appindexing.builders.StickerBuilder;
import com.google.firebase.appindexing.builders.StickerPackBuilder;
import com.google.gson.Gson;
import com.unity3d.player.UnityPlayer;

import org.xmlpull.v1.XmlPullParser;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class VulcanStickerAppIndexingService extends JobIntentService
{
    // Key for checking if the stickers were installed
    private static final String STICKER_INSTALLED_KEY = "VulcanStickersInstalled-%s.version.%d";

    // Patterns for getting the stickers for the pack
    private static final String STICKER_URL_PATTERN = "mystickers://sticker/%s";
    private static final String STICKER_PACK_URL_PATTERN = "mystickers://sticker/pack/%s";

    // Tag for log events
    private static final String TAG = "VulcanStickers";

    // Job-ID must be unique across your whole app.
    private static final int UNIQUE_JOB_ID = 42;

    /**
     * Enqueues the process that will add the stickers to the GBoard from the built-in file.
     */
    public static void EnqueueStickerIndexingFromXMLFile()
    {
        try
        {
            int resourceID = UnityPlayer.currentActivity.getResources().getIdentifier("defaultstickerpack", "xml", UnityPlayer.currentActivity.getPackageName());
            if (resourceID == 0)
            {
                Log.e(TAG, "XML for stickers was not found!");
                return;
            }

            XmlPullParser pullParser = UnityPlayer.currentActivity.getResources().getXml(resourceID);

            // Date we're trying to parse
            int packVersion = 0;
            String packName = null;
            String packDescription = null;
            int packFeaturedIndex = 0;

            ArrayList<VulcanSticker> parsedStickers = new ArrayList<VulcanSticker>();

            // Read the XML file for our data
            int eventType = pullParser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG)
                {
                    final String tagName = pullParser.getName();
                    if (tagName.equals("stickerpack"))
                    {
                        // Get the sticker pack information
                        for (int i = 0; i < pullParser.getAttributeCount(); ++i)
                        {
                            final String attributeName = pullParser.getAttributeName(i);
                            final String attributeValue = pullParser.getAttributeValue(i);

                            if (attributeName.equals("version"))
                            {
                                packVersion = Integer.parseInt(attributeValue);
                            }
                            else if (attributeName.equals("packName"))
                            {
                                packName = attributeValue;
                            }
                            else if (attributeName.equals("packDescription"))
                            {
                                packDescription = attributeValue;
                            }
                            else if (attributeName.equals("featuredStickerIndex"))
                            {
                                packFeaturedIndex = Integer.parseInt(attributeValue);
                            }
                        }
                    }
                    else if (tagName.equals("sticker"))
                    {
                        // This is a sticker!
                        String stickerName = null;
                        String imageURL = null;
                        String[] keyWords = null;

                        for (int i = 0; i < pullParser.getAttributeCount(); ++i)
                        {
                            final String attributeName = pullParser.getAttributeName(i);
                            final String attributeValue = pullParser.getAttributeValue(i);

                            if (attributeName.equals("name"))
                            {
                                stickerName = attributeValue;
                            }
                            else if (attributeName.equals("imageURL"))
                            {
                                imageURL = attributeValue;
                            }
                            else if (attributeName.equals("keyWords"))
                            {
                                keyWords = attributeValue.split(",");
                            }
                        }

                        parsedStickers.add(new VulcanSticker(stickerName, imageURL, keyWords));
                    }
                }

                eventType = pullParser.next();
            }

            // Finally, add in the sticker pack
            if (parsedStickers.size() != 0)
            {
                VulcanSticker[] stickersArray = new VulcanSticker[parsedStickers.size()];
                for (int i = 0; i < parsedStickers.size(); ++i)
                {
                    stickersArray[i] = parsedStickers.get(i);
                }

                VulcanStickerPack stickerPack = new VulcanStickerPack(packVersion, packName, packDescription, packFeaturedIndex, stickersArray);
                EnqueueStickerIndexing(new Gson().toJson(stickerPack));
            }
        }
        catch (Exception e)
        {
            Log.e(TAG, "Failed to parse sticker pack XML", e);
        }
    }

    /**
     * Enqueues the process that will add the stickers to the GBoard
     *
     * @param argStickersJSON JSON of the stickers we want to add
     */
    public static void EnqueueStickerIndexing(String argStickersJSON)
    {
        // Don't do anything if there is no JSON string
        if (TextUtils.isEmpty(argStickersJSON))
        {
            return;
        }

        // Put the JSON of the stickers in the intent
        final Intent stickersIntent = new Intent();
        stickersIntent.putExtra("stickersJSON", argStickersJSON);

        // Enqueue the process
        enqueueWork(UnityPlayer.currentActivity, VulcanStickerAppIndexingService.class, UNIQUE_JOB_ID, stickersIntent);
    }

    /**
     * Handles the work once our process has been kicked off
     *
     * @param argIntent Intent that was used to kick off the process
     */
    @Override
    protected void onHandleWork(@NonNull Intent argIntent)
    {
        final String stickersJSON = argIntent.getStringExtra("stickersJSON");
        final VulcanStickerPack stickerPack = new Gson().fromJson(stickersJSON, VulcanStickerPack.class);

        // Check if the stickers have already been installed or not
        Context appContext = UnityPlayer.currentActivity;
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(appContext);

        if (sharedPreferences.getBoolean(String.format(STICKER_INSTALLED_KEY, stickerPack.GetPackName(), stickerPack.GetVersion()), false) == false)
        {
            InstallStickers(appContext, FirebaseAppIndex.getInstance(), stickerPack);
        }
    }

    /**
     * Installs the give sticker pack.
     * @param argContext Context from which to install from.
     * @param argFirebaseAppIndex Firebase instance to do the indexing.
     * @param argStickerPack Sticker pack to install.
     */
    public static void InstallStickers(final Context argContext, FirebaseAppIndex argFirebaseAppIndex, final VulcanStickerPack argStickerPack)
    {
        try
        {
            // Create the stickers and the stickers pack
            List<Indexable> stickers = GetIndexableStickers(argContext, argStickerPack);
            Indexable stickerPack = GetIndexableStickerPack(argContext, argStickerPack);

            List<Indexable> indexables = new ArrayList<>(stickers);
            indexables.add(stickerPack);

            Task<Void> task = argFirebaseAppIndex.update(
                    indexables.toArray(new Indexable[indexables.size()]));

            task.addOnSuccessListener(new OnSuccessListener<Void>()
            {
                @Override
                public void onSuccess(Void aVoid)
                {
                    // Mark that we have installed the stickers
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(argContext);
                    sharedPreferences.edit().putBoolean(String.format(STICKER_INSTALLED_KEY, argStickerPack.GetPackName(), argStickerPack.GetVersion()),true).commit();

                    Log.v(TAG, "STICKERS - Successfully added stickers");
                }
            });

            task.addOnFailureListener(new OnFailureListener()
            {
                @Override
                public void onFailure(@NonNull Exception e)
                {
                    // Stickers could not be installed
                    Log.d(TAG, "STICKERS - Failed to install stickers", e);
                }
            });
        }
        catch (IOException | FirebaseAppIndexingInvalidArgumentException e)
        {
            // An exception was thrown
            Log.e(TAG, "STICKERS - Unable to set stickers", e);
        }
    }

    /**
     * This method builds the individual stickers.
     * @param argContext Context to install from.
     * @param argStickerPack Sticker pack definition.
     * @return The stickers.
     * @throws IOException Exception can be thrown if directory cannot be made.
     */
    private static List<StickerBuilder> GetStickerBuilders(Context argContext, final VulcanStickerPack argStickerPack) throws IOException
    {
        List<StickerBuilder> stickerBuilders = new ArrayList<>();

        File stickersDir = new File(argContext.getFilesDir(), "stickers");

        if (stickersDir.exists() == false && stickersDir.mkdirs() == false)
        {
            throw new IOException("Stickers directory does not exist");
        }

        final VulcanSticker[] stickers = argStickerPack.GetStickers();
        for (int i = 0; i < stickers.length; i++)
        {
            String stickerFilename = stickers[i].GetName();
            String[] keywords = stickers[i].GetKeywords();

            StickerBuilder stickerBuilder = Indexables.stickerBuilder()
                    .setName(stickerFilename)
                    // Firebase App Indexing unique key that must match an intent-filter
                    // see: Support links to your app content section
                    // (e.g. mystickers://sticker/0)
                    .setUrl(String.format(STICKER_URL_PATTERN, i))
                    // http url or content uri that resolves to the sticker
                    // (e.g. http://www.google.com/sticker.png or content://some/path/0)
                    .setImage(stickers[i].GetImageUrl())
                    .setDescription("content description")
                    .setIsPartOf(Indexables.stickerPackBuilder()
                            .setName(argStickerPack.GetPackName()))
                    .setKeywords(keywords);
            stickerBuilders.add(stickerBuilder);
        }

        return stickerBuilders;
    }

    /**
     * Builds the index of the stickers.
     * @param argContext Context to install from.
     * @param argStickerPack Sticker definitions.
     * @return The indexable stickers
     * @throws IOException Throws exception if directory cannot be made.
     */
    private static List<Indexable> GetIndexableStickers(Context argContext, final VulcanStickerPack argStickerPack) throws IOException
    {
        List<Indexable> indexableStickers = new ArrayList<>();
        List<StickerBuilder> stickerBuilders = GetStickerBuilders(argContext, argStickerPack);

        for (StickerBuilder stickerBuilder : stickerBuilders) {
            stickerBuilder
                    .setIsPartOf(Indexables.stickerPackBuilder()
                            .setName(argStickerPack.GetPackName()));
            indexableStickers.add(stickerBuilder.build());
        }

        return indexableStickers;
    }

    /**
     * Creates the sticker pack.
     * @param argContext Context to install from.
     * @param argStickerPack Sticker definitions.
     * @return The sticker pack.
     * @throws IOException Can throw an exception if directories cannot be made.
     * @throws FirebaseAppIndexingInvalidArgumentException Throws exception if stickers cannot be indexed.
     */
    private static Indexable GetIndexableStickerPack(Context argContext, final VulcanStickerPack argStickerPack)
            throws IOException, FirebaseAppIndexingInvalidArgumentException
    {
        List<StickerBuilder> stickerBuilders = GetStickerBuilders(argContext, argStickerPack);
        File stickersDir = new File(argContext.getFilesDir(), "stickers");

        if (stickersDir.exists() == false && stickersDir.mkdirs() == false)
        {
            throw new IOException("Stickers directory does not exist");
        }

        // Use this to create the icon
        final int featuredIndex = argStickerPack.GetFeaturedStickerIndex();
        final String imageUrl = argStickerPack.GetStickers()[featuredIndex].GetImageUrl();

        StickerPackBuilder stickerPackBuilder = Indexables.stickerPackBuilder()
                .setName(argStickerPack.GetPackName())
                // Firebase App Indexing unique key that must match an intent-filter.
                // (e.g. mystickers://sticker/pack/0)
                .setUrl(String.format(STICKER_PACK_URL_PATTERN, featuredIndex))
                // Defaults to the first sticker in "hasSticker". Used to select between sticker
                // packs so should be representative of the sticker pack.
                .setImage(imageUrl)
                .setHasSticker(stickerBuilders.toArray(new StickerBuilder[stickerBuilders.size()]))
                .setDescription(argStickerPack.GetPackDescription());
        return stickerPackBuilder.build();
    }
}
